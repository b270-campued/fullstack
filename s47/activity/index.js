// console.log("Hello Full Stack!")


const txtFirstName = document.querySelector("#txt-first-name");
const txtLastName = document.querySelector("#txt-last-name");
const spanFullName = document.querySelector("#span-full-name");


let updateFullName = () => {
  const firstName = txtFirstName.value;
  const lastName = txtLastName.value;
  spanFullName.innerHTML = firstName + ' ' + lastName;
}

txtFirstName.addEventListener('keyup', () => {
  updateFullName();
});

txtLastName.addEventListener('keyup', () => {
  updateFullName();
});