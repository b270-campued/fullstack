// console.log("Hello Full Stack!")

// Document Object Model (DOM) using JavaScript

/*
	-In CSS sheets has a concept of box model, JS has its own concept called document object model.
	-In JS, each element in a web page(document) such as images and text inputs can be considered object.
	-Remember, an object in JS contain information about given object (properties and action)
	-With DOM, we can easily access and manipulate tags, IDs, classes, Attributes, or Elements of 
	HTML using commands or methods provided by the Document object.
*/

//Selecting HTML Elements (document.querySelector)
//Syntax: document.querySelector("htmlElement")

	/*
 		document - refers to the whole page
 		.querySelector - used to select a specific object/HTML element from the document
	*/

	
	/*
	Alternatively we can use the getElemet function to retrieve the elements

	document.getElementbyId();
	document.getElementsByClassName();
	document.getElementsByTagName();

	However using these function requires us to identify beforehand how we get the elements.
	With query selector we can be flexible to retrieve the elements
	*/
const txtFirstName = document.querySelector("#txt-first-name");
const txtLastName = document.querySelector("#txt-last-name");
const spanFullName = document.querySelector("#span-full-name");


//[SECTION] Event Listeners

/*
	-Whenever a user interacts with a web page, this action is considered as event
	-The method used is addEventListener. It takes 2 arguments:
		1. A string identifying the event. (ex. keyup = keyboard, Onclick). 
		2. A function that the listener will execute once the event is triggered

	Links: 
	https://www.javascripttutorial.net/javascript-dom/
	https://developer.mozilla.org/en-US/docs/Web/API/Document_Object_Model/Introduction
*/

txtFirstName.addEventListener('keyup', (event) => {

	// The "innerHTML" property sets or returns the value of the element
    // The ".value" property sets or returns the value of an attribute. (
    // form control elements: input, select, etc.)

	spanFullName.innerHTML = txtFirstName.value;
})

// Multiple Listeners can also be assigned to the same event

txtLastName.addEventListener("keyup", (event) => {

	// event.target contains the element where the event happened
	console.log(event.target);

	// event.target.value gets the value of the input
	console.log(event.target.value);
})


// Mini-Activity

txtLastName.addEventListener("keyup", (event) => {

	spanFullName.innerHTML = txtLastName.value;
})


/*function updateFullName() {
  const firstName = txtFirstName.value;
  const lastName = txtLastName.value;
  spanFullName.innerHTML = firstName + ' ' + lastName;
}

txtFirstName.addEventListener('keyup', () => {
  updateFullName();
});

txtLastName.addEventListener('keyup', () => {
  updateFullName();
});*/