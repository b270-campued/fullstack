// Write the queue functions below.
let collection = {};
let frontIndex = 0;
let rearIndex = 0;

function print() {
  const result = [];
  let count = 0;
  for (let i = frontIndex; i < rearIndex; i++) {
    result[count] = collection[i];
    count++;
  }
  return result;
}

function enqueue(element) {
  collection[rearIndex] = element;
  rearIndex++;
  return print();
}

function dequeue() {
  if (frontIndex === rearIndex) {
    return print();
  }
  delete collection[frontIndex];
  frontIndex++;
  return print();
}

function front() {
  return collection[frontIndex];
}

function size() {
  return rearIndex - frontIndex;
}

function isEmpty() {
  return frontIndex === rearIndex;
}


module.exports = {
	print,
	enqueue,
	dequeue,
	front,
	size,
	isEmpty
};