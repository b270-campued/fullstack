// console.log("Hello Again!")

// Reactive DOM with Fetch API

// fetch()
	// this is a mthod in JS that is used to send requests to the server and load responses from the server in the webpages

// Show posts
const showPosts = (posts) => {

	// create a variable that will contain all the posts
	let postEntries = "";

	//forEach() is used to loop each post in our posts mock database
	posts.forEach((post) => {
		console.log(post);

		postEntries += `
			<div id="post-${post.id}">
				<h3 id="post-title-${post.id}">${post.title}</h3>
				<p id="post-body-${post.id}">${post.body}</p>
				<button onclick="editPost('${post.id}')">Edit</button>
				<button onclick="deletePost('${post.id}')">Delete</button>
			</div>
		`;
	});

	document.querySelector("#div-post-entries").innerHTML = postEntries;
}

// Get post data
fetch("https://jsonplaceholder.typicode.com/posts").then((response) => response.json()).then((data) => showPosts(data));

// Add a post

// We select the Add form using the querySelector
// We listen with the submit button for events

document.querySelector("#form-add-post").addEventListener("submit", (event) => {

	// Prevents the page from loading
	event.preventDefault();

	fetch("https://jsonplaceholder.typicode.com/posts", {
		method: "POST",
		headers: {"Content-Type": "application/json"},
		body: JSON.stringify({
			title: document.querySelector("#txt-title").value,
			body: document.querySelector("#txt-body").value,
			userId: 1
		})
	})
	.then((response) => response.json())
	.then((data) => {
		console.log(data);
		alert("Successfully added!")

		// Clear the form
        document.querySelector("#txt-title").value = "";
        document.querySelector("#txt-body").value = "";
	})
})

// Edit a post
const editPost = (postId) => {
  fetch(`https://jsonplaceholder.typicode.com/posts/${postId}`)
    .then((response) => response.json())
    .then((data) => {
    	console.log(data);
    	alert("Successfully updated!")

	      document.querySelector("#txt-edit-id").value = data.id;
	      document.querySelector("#txt-edit-title").value = data.title;
	      document.querySelector("#txt-edit-body").value = data.body;
	      document.querySelector("#btn-submit-update").disabled = false;

	      /*
			let title = documentSelector(`#post-title-${postId}`).innerHTML;
			let body = documentSelector(`#post-body-${postId}`).innerHTML;

			document.querySelector("#txt-edit-id").value = postId;
	        document.querySelector("#txt-edit-title").value = title;
	        document.querySelector("#txt-edit-body").value = body;
	        document.querySelector("#btn-submit-update").removeAttribute("disabled");


	      */
    });
};

// Update a post
document.querySelector("#form-edit-post").addEventListener("submit", (event) => {

    event.preventDefault();

    const postId = document.querySelector("#txt-edit-id").value;

    fetch(`https://jsonplaceholder.typicode.com/posts/${postId}`, {
      method: "PUT",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({
        id: postId,
        title: document.querySelector("#txt-edit-title").value,
        body: document.querySelector("#txt-edit-body").value,
        userId: 1,
      }),
    })
    .then((response) => response.json())
    .then((data) => {
    	console.log(data);
        alert("Successfully updated!");

        // Clear the form
        document.querySelector("#txt-edit-id").value = "";
        document.querySelector("#txt-edit-title").value = "";
        document.querySelector("#txt-edit-body").value = "";
        document.querySelector("#btn-submit-update").disabled = true; //.setAttribute("disabled", true)

        // Update the posts display Note: add this if you can access the files
        /*fetch("https://jsonplaceholder.typicode.com/posts")
          .then((response) => response.json())
          .then((data) => showPosts(data));*/
    });
});


//[ACTIVITY]
// Delete a post
const deletePost = (postId) => {
  fetch(`https://jsonplaceholder.typicode.com/posts/${postId}`, {
    method: "DELETE",
  })
    .then((response) => response.json())
    .then((data) => {
      console.log(data);
      alert("Successfully deleted!");

      // Update the posts display Note: add this if you can access the files
      /*fetch("https://jsonplaceholder.typicode.com/posts")
        .then((response) => response.json())
        .then((data) => showPosts(data));*/
    });
};





