//Import is same as require (to use the package)

import React from 'react';
import ReactDOM from 'react-dom/client';
// mother component
import App from './App';

//Import Bootstrap
import 'bootstrap/dist/css/bootstrap.min.css';


// createRoot assigns element to be managed by REACT with its virtual DOM
const root = ReactDOM.createRoot(document.getElementById('root')); // select ID with root

// render() -displays the react elements/components into the root
root.render(
  <React.StrictMode>
  {/*this is App.js file comment out of jsx*/} 
    <App /> 
  </React.StrictMode>
);

// const name = "John Smith";
// const user = {
//   firstName: "Jane",
//   lastName: "Smith"
// }

// function formatName(user) {
//   return user.firstName + " " + user.lastName;
// }

// // const element = <h1>Hello, {name}</h1> //jsx code
// const element = <h1>Hello, {formatName(user)}</h1> //jsx code

// const root = ReactDOM.createRoot(document.getElementById('root'));
// root.render(element)
