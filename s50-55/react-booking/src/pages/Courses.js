import { Fragment, useState, useEffect } from 'react'
// import coursesData from '../data/coursesData';
import CourseCard from '../components/CourseCard'

export default function Courses() {
	//Checking if mock data was captured
	// console.log(coursesData)

	const [courses, setCourse] = useState([])

	//"map" methof loops through individual course in our mock database and returns a CourseCard component for each course
	// const courses = coursesData.map(course => {
		/*
			courseProp is used to pull data
			With this we can use course card design and put different data
		*/
	// 	return (
	// 		// For react to know updates through ID
	// 		<CourseCard key={course.id} courseProp={course}/>
	// 	)
	// })

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/courses`)
		.then(res => res.json())
		.then(data => {
			console.log(data)
			

			setCourse(data.map(course => {
				console.log(course);
				return(
					<CourseCard key={course._id} courseProp={course}/>
				)
			}))
		})
	},[])

	return (
		<Fragment>
		<h1>Courses</h1>
		{courses}
		</Fragment>
	)
}