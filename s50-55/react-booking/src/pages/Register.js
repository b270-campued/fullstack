import { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap'
import { Navigate, useNavigate } from 'react-router-dom'
import UserContext from '../UserContext'
import Swal from 'sweetalert2';



export default function Register() {

	const navigate = useNavigate();

    const { user } = useContext(UserContext);

	const [ firstName, setFirstName ] = useState("");
	const [ lastName, setLastName ] = useState("");
	const [ mobileNo, setMobileNo ] = useState("");


	const [ email, setEmail ] = useState("");
	const [ password1, setPassword1 ] = useState("");
	const [ password2, setPassword2 ] = useState("");
	// Set to determine whether submit button is enabled or not
	const [ isActive, setIsActive ] = useState(false);

	console.log(email);
	console.log(password1);
	console.log(password2);

	useEffect(() => {
	  // Validation
	  const isEmailValid = email !== "";
	  const isPasswordValid = password1 !== "" && password2 !== "" && password1 === password2;
	  const isFirstNameValid = firstName !== "";
	  const isLastNameValid = lastName !== "";
	  const isMobileNoValid = mobileNo.length === 11;

	  if (isEmailValid && isPasswordValid && isFirstNameValid && isLastNameValid && isMobileNoValid) {
	    setIsActive(true);
	  } else {
	    setIsActive(false);
	  }
	}, [email, password1, password2, firstName, lastName, mobileNo]);


	// Function to simulate user registration
	function registerUser(e) {
		e.preventDefault();

		// Check if the email already exists
		fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail`, {
		    method: "POST",
		    headers: {
		      "Content-Type": "application/json",
		    },
		    body: JSON.stringify({
		      email: email
		    }),
		})
		.then(res => res.json())
		.then(emailExists => {
			if (emailExists) {
		        // Email already exists, show an error
		        Swal.fire({
		          title: "Duplicate email found",
		          icon: "error",
		          text: "Please provide a different email.",
		        });
			} else {

				// Email does not exist, proceed with user registration
				fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
					method: "POST",
					headers: {
						"Content-Type" : "application/json",
					},
					body: JSON.stringify({
						//password1 with the other values are the one from front end
						firstName: firstName,
						lastName: lastName,
						mobileNo: mobileNo,
						email: email,
						password: password1,

					})
				})
				.then(res=> res.json())
				.then(data => {
					console.log(data);

					if(data) {

						// Registration successful
						Swal.fire({
							title: "Registration Successful",
							icon: "success",
							text: "Welcome to Zuitt!"
						})

						navigate("/login"); //Go  to login after successfully registered

					} else {

						// Registration failed
						Swal.fire({
							title: "Something went wrong",
							icon: "error",
							text: "Please try again."
						});
					}
				})
			}
		})


		
		//Clear input fields
		setFirstName("");
		setLastName("");
		setMobileNo("");
		setEmail("");
		setPassword1("");
		setPassword2("");

		/*alert("Thank you for registering!")*/

	}

	return (
		(user.id !== null) /*changed to user.id vs user.email*/
		?
		<Navigate to="/courses"/>
		:
		<Form onSubmit={(e) => registerUser(e)}>

			<Form.Group className="mb-3" controlId="firstName">
				<Form.Label>First Name</Form.Label>
				<Form.Control 
					type="text" 
					placeholder="Enter First Name"
					required
					value={firstName}
					onChange={e => setFirstName(e.target.value)} 
				/>

			</Form.Group>

			<Form.Group className="mb-3" controlId="lastName">
				<Form.Label>Last Name</Form.Label>
				<Form.Control 
					type="text" 
					placeholder="Enter Last Name"
					required
					value={lastName}
					onChange={e => setLastName(e.target.value)} 
				/>

			</Form.Group>

			<Form.Group className="mb-3" controlId="mobileNo">
				<Form.Label>Mobile Number</Form.Label>
				<Form.Control 
					type="text" 
					placeholder="Enter Your Mobile Number"
					required
					value={mobileNo}
					onChange={e => setMobileNo(e.target.value)} 
				/>

				<Form.Text className="text-muted">
				  We'll never share your mobile number with anyone else.
				</Form.Text>

			</Form.Group>

			<Form.Group className="mb-3" controlId="userEmail">
				<Form.Label>Email address</Form.Label>
				<Form.Control 
					type="email" 
					placeholder="Enter email"
					required
					value={email}
					onChange={e => setEmail(e.target.value)} 
				/>

				<Form.Text className="text-muted">
				  We'll never share your email with anyone else.
				</Form.Text>

			</Form.Group>

			<Form.Group className="mb-3" controlId="password1">
				<Form.Label>Password</Form.Label>
				<Form.Control 
					type="password" 
					placeholder="Password" 
					required
					value={password1}
					onChange={e => setPassword1(e.target.value)} 
				/>
			</Form.Group>

			<Form.Group className="mb-3" controlId="password2">
				<Form.Label>Verify Password</Form.Label>
				<Form.Control 
					type="password" 
					placeholder="Verify Password" 
					required
					value={password2} /*what shows in front end*/
					onChange={e => setPassword2(e.target.value)} 
				/>
			</Form.Group>
			
			{/*Conditional renderred submit button based on isActive states*/}
			{isActive ? <Button variant="primary" type="submit">Register</Button> : <Button variant="danger" type="submit" disabled>Register</Button>}

		</Form>
	)
}