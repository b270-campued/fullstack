import { useContext, useEffect } from 'react'
import { Navigate } from 'react-router-dom';
import UserContext from '../UserContext'


export default function Logout() {
	
	const { unsetUser, setUser } = useContext(UserContext);

	// Clear local storage
	unsetUser();
	
	// localStorage.clear()


	useEffect(() => {
		/*changed to id from email*/
		setUser({id: null}) 
	})

	// Redirect to login
	return(
		<Navigate to='/login' />
	)

} 
