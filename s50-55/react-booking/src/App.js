import './App.css';
// Wrap mulitple components
// import { Fragment } from 'react';
import { useState, useEffect } from 'react';
import { Container } from 'react-bootstrap';
import { BrowserRouter as Router } from 'react-router-dom';
import { Route, Routes } from 'react-router-dom';
import AppNavbar from './components/AppNavbar';
import Home from './pages/Home';
import Courses from './pages/Courses';
import CourseView from './pages/CourseView';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import NotFound from './pages/NotFound';
import { UserProvider } from './UserContext';


function App() {

  //Global state hook for the user information for validating if a user is logged in

  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  });

  const unsetUser = () => {
    localStorage.clear();
  }


  // Used to check if the user info is properly stored upon login and if the local storage is cleared upon logout
  useEffect(() => {
    console.log(user);
    console.log(localStorage)
  }, [user])

  //
  useEffect(() => {
      fetch("http://localhost:4000/users/details", {
          headers: {
              Authorization: `Bearer ${localStorage.getItem('token')}`
          }
      })
      .then(res => res.json())
      .then(data => {
         
         if(data._id !== undefined) {

            // Sets the use state values with the user details upon successful login
            setUser({
                id: data._id,
                isAdmin: data.isAdmin
            });

          // Sts the user state to the initial value 
         } else {
            setUser({
                id: null,
                isAdmin: null
            });
         }
          
      });
    }, []);

  return (
    <UserProvider value={{user, setUser, unsetUser}}>
      <Router>

        <Container fluid>

          <AppNavbar />

          <Routes>
            <Route path="/" element={<Home />} />
            <Route path="/courses" element={<Courses />} />
            <Route path="/courses/:courseId" element={<CourseView />} />
            <Route path="/register" element={<Register />} />
            <Route path="/login" element={<Login />} />
            <Route path="/logout" element={<Logout />} />
            <Route path="*" element={<NotFound />} /> {/* Route for unmatched routes */}
          </Routes>

        </Container>
      </Router>

    </UserProvider>


  );
}



export default App;

