// import { useState, useEffect } from 'react';
import { Card, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';

/*Parameters are considered objects {}*/
export default function CourseCard({courseProp}) {

	console.log(courseProp);


	/*Object Deconstruction*/
	// Deconstruct the course properties into their own variables
	const { _id, name, description, price } = courseProp;

	// Syntax ; const [getter, setter] = useState(iniitalGetterValue)
	// setCount and setSeats is already asyn function
	// use state hook which are react methods
	// const [ count, setCount ] = useState(0);
	// const [ seats, setSeats ] = useState(30);
	// const [ isOpen, setIsOpen ] = useState(false);

	// function enroll() {
	// 		setCount(count + 1);
    // 		setSeats(seats - 1);
    // 		console.log('Enrollees ' + count);
    // 		console.log('Seats ' + seats);
	// }

	//For disabling of button
	// useEffect(() => {
	// 	if (seats === 0 ) {
	// 		setIsOpen(true);
	// 	}
	// }, [seats]); /*The [seats] array specifies that the effect should only run when the seats value changes.*/

	/*
		The useEffect hook in React accepts two parameters:

		The first parameter is a callback function that contains the code you want to run as the 
		effect. This function will be executed after the component has rendered or 
		when the dependencies specified in the second parameter have changed.

		The second parameter is an optional array of dependencies. It determines when the effect 
		should be run. If the dependencies array is empty, the effect will only run once after 
		the initial render. If there are dependencies specified, the effect will run when any 
		of the dependencies change.
	*/

	return (
		
		<Card>
		      <Card.Body>
		      	<Card.Title>{name}</Card.Title>
		      	<Card.Subtitle>Description</Card.Subtitle>
		        <Card.Text>{description}</Card.Text>
		        <Card.Subtitle>Price:</Card.Subtitle>
		        <Card.Text>PhP {price}</Card.Text>
		        <Button variant="primary"  as={Link} to={`/courses/${_id}`}>Details</Button>
		      </Card.Body>
		 </Card>
	)
}