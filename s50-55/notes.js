// React - Component Driven Development

/*
	What is React.js
	-JS library for building user interfaces that is:

	1) Declarative : It makes you write code with the question of WHAT
	instead of HOW something is to be done. It also allows code to be more 
	predictable and easier to debug.
	2) Component-based : It allows you ti write reusable,complex UI in a quick and effecient manage (Single Page Application)
	3) Learn Once and Write Anywhere : can be used in the server using Node and power mobile apps using react native.

	What problem that is solved?
	-Separate concerns into  components rather than technologies instead if conventional
	HTML for structure, CSS for styling and JS for interactivity

	-Apply rapid updates using  VIRTUAL DOM. This only works on the diffences between current DOM state and
	target DOM state.
	e.g Change the light bulb without tearing down the house and building it backup.


	-Store info in a component using states
	e.g traffic light is component and its states are stop,wait and go.
*/

//React Props and States - how data is managed and passed between react components

/*
	Props - short for properties and are informations that a component receives usually from parent component
	Props are synonymous to function parameters
	Props are what allow components to be reusable
	By using props, once can use same component and feed different data to the component rendering
*/

/*
	States allow components to create and manage its own data and is meant to used internally
	When creating states we used HOOKS
	Hook in react are functions that allow developers to create and manage states and lifecyle within components
*/

// Routing and Conditional Rendering

/*
	React Routing
	
	React.js routing refers to the process of managing different views or components in a React 
	application based on the URL or user navigation. 

	React Router is a popular library used for implementing routing in React applications.

	React Router provides a set of components that enable developers to define the routing 
	configuration and map specific URLs to different React components. These components include:

	1. `<BrowserRouter>`: This component is used to set up the routing environment and uses 
	HTML5 history API for navigation. It's typically placed at the root level of the application.

	2. `<Route>`: The `<Route>` component defines a mapping between a URL path and a corresponding 
	React component. It renders the specified component when the URL matches the defined path.

	3. `<Switch>`: The `<Switch>` component is used to render only the first matching `<Route>` 
	component. This is useful when you want to ensure that only one component is rendered at a time.

	4. `<Link>`: The `<Link>` component is used to create links to different routes in your application. 
	It renders an anchor tag (`<a>`) with the specified URL, allowing users to navigate to different 
	views without a full page reload.

	5. `<Redirect>`: The `<Redirect>` component is used to redirect users from one route to another. 
	It can be used in response to certain conditions or events.

	By defining routes using these components, developers can create a navigation structure in 
	their React applications. When a user navigates to a specific URL, the corresponding 
	React component is rendered, allowing for dynamic and interactive user experiences.


 	Install Router
 	terminal > npm install react-router-dom
	
*/

/*
	App State Management
	Manage and pass data within a REACT JS app

*/

/*
	env.local
	-Purpose to hide sensitive information like API URL}

	REACT_APP_API_URL=http://localhost:4000 - this is for local
	REACT_APP_API_URL=<API LINK DEPLOYED>


*/