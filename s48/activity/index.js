// console.log("Hello Again!")

// Interactive Page with JSON Data

//Mock database

let posts = [];

// Post ID
let count = 1;

// Add post
document.querySelector("#form-add-post").addEventListener("submit", (event) => {

	// Prevents the page from loading
	event.preventDefault();

	posts.push({
		id:count,
		//Title and body values will come from the input elements in the form
		title: document.querySelector("#txt-title").value,
		body: document.querySelector("#txt-body").value
	});
	// Count will increment everytime a new post is posted
	count++;

	console.log(posts)

	alert("Successfully added!");

	showPosts(posts);

})


// Show posts
const showPosts = (posts) => {

	// create a variable that will contain all the posts
	let postEntries = "";

	//forEach() is used to loop each post in our posts mock database
	posts.forEach((post) => {
		console.log(post);

		postEntries += `
			<div id="post-${post.id}">
				<h3 id="post-title-${post.id}">${post.title}</h3>
				<p id="post-body-${post.id}">${post.body}</p>
				<button onclick="editPost('${post.id}')">Edit</button>
				<button onclick="deletePost('${post.id}')">Delete</button>
			</div>
		`;
	});

	document.querySelector("#div-post-entries").innerHTML = postEntries;
}


// Edit post
// This will trigger an event that will update a certain post upon clicking the edit button
const editPost = (postId) => {
	const postTitle = document.querySelector(`#post-title-${postId}`).innerHTML;
	const postBody = document.querySelector(`#post-body-${postId}`).innerHTML;

	document.querySelector("#txt-edit-id").value = postId;
	document.querySelector("#txt-edit-title").value = postTitle;
	document.querySelector("#txt-edit-body").value = postBody;
};

// Update post
document.querySelector("#form-edit-post").addEventListener("submit", (event) => {
	
	event.preventDefault();

	// Alternative code

		/*const postId = document.querySelector("#txt-edit-id").value;
		const updatedTitle = document.querySelector("#txt-edit-title").value;
		const updatedBody = document.querySelector("#txt-edit-body").value;

		// The findIndex() method is an array method in JavaScript that 
		// returns the index of the first element in an array that 
		// satisfies a given condition. 
		// It iterates over each element in the array and executes a 
		// callback function, checking if the element meets the specified condition. 
	    // Once the condition is met, findIndex() returns the index of that element. 
	    // If no element satisfies the condition, it returns -1.

		const postIndex = posts.findIndex((post) => post.id === parseInt(postId));
		
		if (postIndex !== -1) {
			posts[postIndex].title = updatedTitle;
			posts[postIndex].body = updatedBody;

			console.log(posts);

			alert("Successfully updated!");

			showPosts(posts);
		} else {
			alert("Post not found!");
		}*/

	for(let i = 0; i < posts.length; i++) {

		if (posts[i].id == document.querySelector("#txt-edit-id").value) {

			posts[i].title = document.querySelector("#txt-edit-title").value;
			posts[i].body = document.querySelector("#txt-edit-body").value;

			showPosts(posts);
			alert("Successfully updated!")
			break;
		}

	}
});




	

// [ACTIVITY]
// Delete post
	// The deletePost function takes postId as a parameter, which represents the ID of the post to be deleted.
	// deletePost('${post.id}')
const deletePost = (postId) => {
	// Prompts a confirmation dialog using the confirm function
	const confirmDelete = confirm("Are you sure you want to delete this post?");
	
	//If the user confirms the deletion (by clicking "OK" in the confirmation dialog), the code inside the if statement is executed.
	if (confirmDelete) {

		// The posts array is updated using the filter method.
		// It creates a new array that only includes the posts where the id is not equal to the postId that was passed as an argument. This effectively removes the post with the specified postId from the array.
		// filter()
			//Returns a new array that contains the elements which meet the condition
			//Returns an empty array if no element met the condition
		// The parseInt() function in JavaScript is used to parse a string and convert it into an integer.
		posts = posts.filter((post) => post.id !== parseInt(postId));

		//Should show the updated posts array
		console.log(posts);

		alert("Successfully deleted!");

		//showPosts function is called to update the displayed list of posts on the page, passing the updated posts array as an argument.
		showPosts(posts);
	}
};